<!DOCTYPE html>
<html>
	<head>
		<!-- Meta Data & Title -->
		<title>gorgonsteel's portfolio - Home</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<meta property="og:site_name" content="gorgonsteel's portfolio" />
		<meta property="og:title" content="gorgonsteel's portfolio" />
		<meta property="og:description" content="I'm gorgonsteel. I do stuff on computers utilizing HTML, CSS and PHP. I do some JS too, but I don't say I know those technologies." />
		<meta property="og:image" content="/assets/img/avatar.png" />
		<meta property="og:url" content="me.gorgonsteel.io" />
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Favicon -->
		<link href="/assets/img/avatar.png" rel="shortcut icon" />
		<link href="/assets/img/avatar.png" rel="apple-touch-icon" />
		<link href="/assets/img/avatar.png" rel="apple-touch-icon" sizes="76x76" />
		<link href="/assets/img/avatar.png" rel="apple-touch-icon" sizes="120x120" />
		<link href="/assets/img/avatar.png" rel="apple-touch-icon" sizes="152x152" />
		<link href="/assets/img/avatar.png" rel="apple-touch-icon" sizes="180x180" />
		<link href="/assets/img/avatar.png" rel="icon" sizes="192x192" />
		<link href="/assets/img/avatar.png" rel="icon" sizes="128x128" />

		<!-- css includes -->
		<link rel="stylesheet" type="text/css" href="/assets/css/main.css" title="main-css" />
		<link rel="stylesheet" type="text/css" href="/assets/css/mc-block.css" media="all" id="minecraft-block-css" />
		<link rel="stylesheet" type="text/css" href="https://gorgonsteel.io/wp-content/plugins/minecraft-block/css/block.css?ver=4.5.2" />
		<link rel="stylesheet" type="text/css" href="https://drewgourley.com/wp-content/themes/drew/style.css" />
		<link rel="stylesheet" type="text/css" href="/assets/css/animate.css" />
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway:400,300,200,700&subset=latin,latin-ext" />
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic&subset=latin,latin-ext" />
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,700,400italic,700italic&subset=latin,latin-ext" />
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,700,400italic,700italic&subset=latin,latin-ext" />
		<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<style type="text/css">
		body {
		  background:url("//i.imgur.com/plCN83H.png") repeat center center fixed;
		}
		.center {
		  position: relative;
		  top: 100%;
		  transform: translateY(25%);
		}
		.logo {
		  padding-top: 30px;
		  padding-bottom: 10px;
		  background-color: #fffff;
		}
		.animation {
		  -webkit-animation-delay: 900ms;
		  animation-delay: 900ms;
		  -moz-animation-delay: 900ms;
		  -ms-animation-delay: 900ms;
		  -o-animation-delay: 900ms;
		}
		.width {
		  max-width: 700px;
		}
		</style>

		<!-- JS Includes -->
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.0/jquery.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
		<script src="/assets/js/wow.min.js"></script>
		<script src="/assets/js/wow-spec.js"></script>
		<script> 
		  new WOW().init(); 
		</script>
	</head>
	<body>
		<div class="container width">
			<center>
				<div class="logo">
					<img src="/assets/img/logo.png">
				</div><br>
			</center>
			<div class="alert alert-info" role="alert" style="border-radius: 0px; !important">
				My main website / blog, gorgonsteel.com was deleted from the server it was hosted on, resulting in loss of all data. 
				I did not have any backup of that website, so i am forced to make a new one. 
				I have decided to use wordpress as CMS because i can"t be bothered to build a new one.
			</div>
			<h2>
				<div>
					<font color="gold">
						About me
					</font>
				</div>
			</h2>
			<div class="paragraph" style="text-align:left;">
				<span style="font-weight:bold">
					<font color="white">
						I'm gorgonsteel. I do stuff on computers utilizing HTML, CSS and PHP.
						I do some JS too, but I don't say I know those technologies. My portofolio can be found below.
						If you want to contact me, please send me an email to <a href="mailto:gorgonsteel@gmail.com" target="_blank"> gorgonsteel@gmail.com</a>
					</font>
					<br><br>
					<h2>
						<font color="gold">
							Portfolio
						</font>
					</h2>
					<font color="red">
						<h4>
							<a href="https://gorgonsteel.io/" target="_blank">
								gorgonsteel Blog
							</a> - My personal blog
						</h4>
						<h4>
								<a href="https://craft-list.net/" target="_blank">
									Craft-List
								</a> - A large minecraft server list
						</h4>
						<h4>
								<a href="https://sharetext.ml/" target="_blank">
									ShareText
								</a> - Share text documents with friends and family in seconds!
						</h4>
						<h4>
								<a href="https://norhood.com/" target="_blank">
									NorHood
								</a> - A small minecraft community
						</h4>
						<h4>
								<a href="https://images.fawdaw.com/" target="_blank">
									Fawdaw Images
								</a> - Fast and easy image hosting site
						</h4>
						<h4>
								<a href="https://netportal.cf/" target="_blank">
									NetPortal
								</a> - A handy little software portal
						</h4>
					</font>
				</span>
			</div>				
		</div><br><br><br><br>
		<div id="footer" class="navbar-fixed-bottom">
			<!-- Minecraft Block by Drew Gourley, http://drewgourley.com/the-minecraft-block-plugin -->
			<div id="minecraft-container">
				<div class="minecraft lapis game">	
					<div class="cube">
						<div class="face six"><a href="https://gorgonsteel.com/"></a></div>
						<div class="face five"><a href="https://gorgonsteel.com/"></a></div>
						<div class="face four"><a href="https://gorgonsteel.com/"></a></div>
						<div class="face one"><a href="https://gorgonsteel.com/"></a></div>
						<div class="face two"><a href="https://gorgonsteel.com/"></a></div>
						<div class="face three"><a href="https://gorgonsteel.com/"></a></div>
					</div>	
				</div>
			</div>
			<br>
			<center>
				<font color="gold">
					<strong>
						Copyright &copy; <?php echo date("Y")?> gorgonsteel.io
					</strong>
				</font>
			</center>
			<br>
		</div>
	</body>
</html>
